# Changelog

This is a changelog for the xbell-ringer program for freedesktop-based
systems. xbell-ringer is a small program that listens for XKB bell events
and produces sounds when they occur. The format of this file is based on
[Keep a Changelog][], and releases adhere to [Semantic Versioning][].

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

<!-- When editing this file, note the use of en dashes in the headings. -->

## (Unreleased)

## 0.1.2 – 2022-07-18

### Fixed

- Fixed incorrect installation location for documentation.

## 0.1.1 – 2022-07-17

### Changed

- The default CFLAGS setting when building should work a bit better and
  enables more warnings by default.

### Fixed

- Fixed potential crash displaying error message if the lock file started
  out locked but the lock information wasn't then available.

## 0.1.0 – 2022-07-17

- Initial release. This should be largely feature-complete for 1.0.0,
  but it will only be considered stable when at least a few users have
  reported success under different desktop environments.

<!--
Local variables:
mode: markdown
fill-column: 76
End:
-->
