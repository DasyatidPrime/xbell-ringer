# This file is part of xbell-ringer. xbell-ringer is free software released
# under the GNU GPL v3 or later. See "Licensing" in README.md for details.
#
# SPDX-License-Identifier: GPL-3.0-or-later

ifeq ($(origin CC),default)
CC = gcc
endif

ifneq (,$(findstring :$(origin CFLAGS):,:default:undefined:))
CFLAGS = -Os -Wall -Wextra
endif

DESTDIR ?=

man1dir := $(mandir)/man1
our_docdir := $(docdir)/xbell-ringer

INSTALL ?= install
INSTALL_DIR ?= $(INSTALL) -d
INSTALL_PROGRAM ?= $(INSTALL) -m 0755
INSTALL_DATA ?= $(INSTALL) -m 0644

exe := xbell-ringer
unit_file := xbell-ringer.service
man1_page := xbell-ringer.1
version_source := xbell-ringer-version.c

clean_files = $(exe)

distclean_files = GNUmakefile config.status xbell-ringer.service
distclean_files += $(version_source)

sources := $(addprefix $(srcdir)/,xbell-ringer.c) $(version_source)

required_cflags = -std=gnu99 $(pc_cflags)
required_libs = $(pc_libs)

macros :=

ifeq ($(with_systemd),true)
macros += -DWITH_SYSTEMD=1
else
macros += -DWITH_SYSTEMD=0
endif

all: $(exe)

$(exe): $(sources)
	$(CC) $(CFLAGS) $(required_cflags) $(macros) -o $(exe) \
		$(sources) $(LIBS) $(required_libs)

install: all
	$(INSTALL_DIR) $(DESTDIR)$(bindir)
	$(INSTALL_DIR) $(DESTDIR)$(our_docdir)
	$(INSTALL_DIR) $(DESTDIR)$(man1dir)
	$(INSTALL_DIR) $(DESTDIR)$(systemduserunitdir)
	$(INSTALL_PROGRAM) -t $(DESTDIR)$(bindir) $(exe)
	$(INSTALL_DATA) -t $(DESTDIR)$(our_docdir) $(srcdir)/README.md
	$(INSTALL_DATA) -t $(DESTDIR)$(man1dir) $(man1_page)
	$(INSTALL_DATA) -t $(DESTDIR)$(systemduserunitdir) $(unit_file)

clean:
	-rm -f -- $(clean_files)

distclean: clean
	-rm -f -- $(distclean_files)

.PHONY: all install clean distclean

dist_ignore_files = $(clean_files) $(distclean_files)
dist_ignore_files += .git\* local

dist_stem := xbell-ringer-$(version)

dist_find_print0 = \
	\( -false $(addprefix -o -path ./,$(dist_ignore_files)) \) -prune \
	-o -not -name $(dist_stem)'.tar.*' \
	-print0

dist_compress := gzip -9
dist_compress_ext := gz
dist_target_file := $(dist_stem).tar.$(dist_compress_ext)

dist-find:
	@( cd $(srcdir) && \
	find $(dist_find_print0) \
		| LC_COLLATE=C sort -z \
		| pax -0Ldwx ustar -M dist -s '!\./!'$(dist_stem)'/!' \
		| $(dist_compress) \
	) >$(dist_target_file)
	@printf 'Wrote %s (find)\n' $(dist_target_file)

dist-git:
	@( cd $(srcdir) && \
	git archive --format=tar --prefix=$(dist_stem)/ HEAD \
		| $(dist_compress) \
	) >$(dist_target_file)
	@printf 'Wrote %s (git)\n' $(dist_target_file)

ifeq (exists,$(shell test -d $(srcdir)/.git && echo exists))
dist: dist-git
else
dist: dist-find
endif

.PHONY: dist dist-find dist-git

# Local variables:
# mode: makefile-gmake
# fill-column: 76
# End:
