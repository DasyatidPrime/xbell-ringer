<!--
This file is part of xbell-ringer. xbell-ringer is free software released
under the GNU GPL v3 or later. See "Licensing" in the top-level README.md
for details.

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Example bells

This directory contains some extra bell sounds you might like if the
one that comes with your desktop sound theme is not to your liking.
Currently, there's just `trapezium-bell`, which is a short, high
bleep, along with a shell script used to synthesize it using SoX.

You can install an audio file somewhere and provide its location using
xbell-ringer's `--file` option, or you can install it as an override
file under your `$XDG_DATA_HOME` (usually `$HOME/.local/share`) using
the structure from the [XDG Sound Theme Specification][].

[XDG Sound Theme Specification]: https://freedesktop.org/wiki/Specifications/sound-theme-spec/

<!--
Local variables:
mode: markdown
fill-column: 76
End:
-->
