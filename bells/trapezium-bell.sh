#!/bin/sh
# This file is part of xbell-ringer. xbell-ringer is free software released
# under the GNU GPL v3 or later. See "Licensing" in the top-level README.md
# for details.
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generates a short, high bleep using SoX.

if [ $# -ne 1 ]; then
    printf 'Usage: %s OUTPUT-FILE\n' "$0" >&2
    exit 64
fi
output_file=$1

sox -R -n -c1 -r48000 -b16 "$output_file" \
    synth 0.050 trapezium 3900 0 0 gain -12 fade 0.010 0.050 \
