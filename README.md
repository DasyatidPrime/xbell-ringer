<!--
This file is part of xbell-ringer. xbell-ringer is free software released
under the GNU GPL v3 or later. See "Licensing" below for details.

SPDX-License-Identifier: GPL-3.0-or-later
-->

# xbell-ringer

(2022-07-15)

xbell-ringer is a small program that listens for XKB bell events. When one
occurs, it issues an XDG Sound Theme event via libcanberra.

## Why?

PulseAudio handles this using [module-x11-bell][], but it doesn't use the
ambient sound theme nor support dynamic toggling. It requires that you
manually configure and load which sample to play, which isn't too bad but
requires fussing with some configuration syntax.

[module-x11-bell]: https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-x11-bell

PipeWire has its own [libpipewire-x11-bell][], which also doesn't use the
ambient sound theme nor support dynamic toggling. It uses libcanberra, but
it doesn't acquire the theming properties from the environment, and it also
recreates the context every time the bell is rung. It also requires fussing
with some configuration syntax.

[libpipewire-x11-bell]: https://docs.pipewire.org/page_module_x11_bell.html

When I switched from PulseAudio to PipeWire, I wanted a bell sound that
wasn't the water-drop-like “bwip” of the default freedesktop theme's bell
sound, but there wasn't an obvious slot for loading my own sample. When I
tried to configure the sound theme, that didn't work either. And so.

## Features

- Watches [XSETTINGS][] to follow desktop settings for theme and toggle
- Holds the audio server connection open (probably)
- Runs smoothly under systemd user session supervision, with startup
  notification support and unit file installed by default

[XSETTINGS]: https://specifications.freedesktop.org/xsettings-spec/0.5/

## Dependencies

- [libcanberra][]
- libsystemd for startup notification
- [xcb][]

[libcanberra]: https://0pointer.de/lennart/projects/libcanberra/
[xcb]: https://xcb.freedesktop.org/

## Extras

The ‘bells’ directory contains an example file that you may find suitable
for use as a more traditional bell sound if you don't like the available
sound themes otherwise.

## Licensing

xbell-ringer is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

xbell-ringer is distributed in the hope that it will be useful, but
**without any warranty**; without even the implied warranty of
**merchantability** or **fitness for a particular purpose**. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along with
xbell-ringer; see the file `LICENSE-GPL3.txt`. If you do not have a copy,
see <https://www.gnu.org/licenses/>.

<!--
Local variables:
mode: markdown
fill-column: 76
End:
-->
