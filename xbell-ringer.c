/*
 * This file is part of xbell-ringer. xbell-ringer is free software released
 * under the GNU GPL v3 or later. See "Licensing" in README.md for details.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/// --- Headers ------------------------------------------------------

#define _GNU_SOURCE 1
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include <canberra.h>
#include <xcb/xcb.h>
#include <xcb/xkb.h>

#if WITH_SYSTEMD
#include <systemd/sd-daemon.h>
#endif


/// --- State variables ----------------------------------------------

/* Description (-d). Static lifetime. */
static char const *event_desc_ = "X11 bell";
/* Sound file name (-f). Static lifetime. */
static char const *event_file_ = NULL;
/* Sound event identifier (-i). Static lifetime.
   Default is set in process_options. */
static char const *event_id_ = NULL;
/* Cache control specification (-c). Static lifetime. */
static char const *cache_control_ = "never";
/* Volume in dbFS. (Backends may ignore this.) */
static double volume_db_ = 0.0;

/* True if the --debug flag is turned on. */
static bool debug_ = false;

/* libcanberra context. */
static ca_context *ca_ = NULL;
/* Property list to apply when playing the bell sound. */
static ca_proplist *bell_proplist_ = NULL;
/* Sound theme name, malloc'd. */
static char *sound_theme_name_ = NULL;
/* Whether event sounds are currently enabled. */
static int enable_event_sounds_ = 1;
/* Set to true when changing either of the above properties. Set to false
   when the libcanberra context is updated to match. */
static bool ca_props_changed_ = false;

/* The default screen number for our X connection. */
static int screen_;
/* xcb-based X connection. */
static xcb_connection_t *xc_ = NULL;
/* Event type assigned to the XKEYBOARD (XKB) extension. */
static uint8_t xkb_event_;

/* The atom _XSETTINGS_Sn, where n is our screen number. */
static xcb_atom_t xsettings_selection_atom_;
/* The atom _XSETTINGS. */
static xcb_atom_t xsettings_property_atom_;
/* The atom _XSETTINGS. */
static xcb_atom_t xsettings_type_atom_;
/* The atom MANAGER. */
static xcb_atom_t manager_atom_;
/* Atoms are allocated fairly early after the X connection is made. */

/* ID of the root window. */
static xcb_window_t root_window_;
/* ID of the current XSETTINGS window, updated through
   set_xsettings_window. */
static xcb_window_t xsettings_window_;


/// --- Support functions --------------------------------------------

/* Return PTR if non-null; otherwise, exit with an error. */
static char *
check_alloc(char *ptr)
{
        if (!ptr) {
                fputs("error: allocation failed\n", stderr);
                exit(EX_OSERR);
        }

        return ptr;
}

/* Like malloc, but exits if SIZE > 0 and the result would be NULL. */
static inline void *
xmalloc(size_t size)
{
        return (size == 0)? NULL : check_alloc(malloc(size));
}

/* Like strdup, but exits if the result would be NULL. */
static inline char *
xstrdup(char const *string)
{
        return check_alloc(strdup(string));
}

/* Write a single-line description of XERR to standard error, terminated
   with a newline. If XERR is NULL, report on whether the ambient X
   connection has a connection-based error instead. */
static void
report_xerr(xcb_generic_error_t *xerr)
{
        int err;

        if (xerr) {
                fprintf(stderr, "X error: rtype=%u error=%u opcode=%u/%u\n",
                        xerr->response_type, xerr->error_code,
                        xerr->major_code, xerr->minor_code);
        } else if (xc_ && (err = xcb_connection_has_error(xc_)) != 0) {
                fprintf(stderr, "X connection error %d\n", err);
        } else {
                fputs("unknown X error\n", stderr);
        }
}

/* Report XERR (an xcb_generic_error_t) to standard error along with a
   formatted error message, then exit. */
#define die_xerr(xerr, fmt, ...) do { \
                fprintf(stderr, "error: " fmt ": ",             \
                        ## __VA_ARGS__);                        \
                report_xerr(xerr);                              \
                exit(EX_IOERR);                                 \
        } while (0)

/* Report a formatted error message to stderr and exit. */
#define die(fmt, ...) do {                                      \
                fprintf(stderr, "error: " fmt "\n",             \
                        ## __VA_ARGS__);                        \
                exit(EX_UNAVAILABLE);                           \
        } while (0)

/* Report XERR (an xcb_generic_error_t) to standard error along with a
   formatted error message. */
#define warn_xerr(xerr, fmt, ...) do {                          \
                fprintf(stderr, "warning: " fmt ": ",           \
                        ## __VA_ARGS__);                        \
                report_xerr(xerr);                              \
        } while (0)

/* Report a formatted error message to stderr. */
#define warn(fmt, ...) \
        fprintf(stderr, "warning: " fmt "\n", ## __VA_ARGS__)

/* Report a formatted error message to stderr including the line number, but
   only if debug_ was turned on. */
#define debug(fmt, ...) do {                                    \
                if (debug_) {                                   \
                        fprintf(stderr, "line %d: " fmt "\n",   \
                                __LINE__, ## __VA_ARGS__);      \
                }                                               \
        } while (0)


/// --- Producing libcanberra events ---------------------------------

/* Ensure a libcanberra context exists and has our relatively static
   properties set on it. */
static void
set_canberra_properties(void)
{
        int err;
        debug("update libcanberra properties");

        if (!ca_) {
                err = ca_context_create(&ca_);
                if (err != 0)
                        die("creating libcanberra context: %s",
                            ca_strerror(err));
        }

        char screen_decimal[24];
        snprintf(screen_decimal, sizeof(screen_decimal), "%d", screen_);
        debug("screen: %s", screen_decimal);

        char enable_decimal[2];
        enable_decimal[0] = enable_event_sounds_? '1' : '0';
        enable_decimal[1] = '\0';
        debug("enable: %s", enable_decimal);

        char volume_db_decimal[24];
        snprintf(volume_db_decimal, sizeof(volume_db_decimal),
                 "%.f", volume_db_);
        debug("volume (dB): %s", volume_db_decimal);

        err = ca_context_change_props(
                ca_,
                CA_PROP_APPLICATION_NAME, "xbell-ringer",
                // TODO: CA_PROP_WINDOW_X11_DISPLAY
                CA_PROP_WINDOW_X11_SCREEN, screen_decimal,
                CA_PROP_CANBERRA_ENABLE, enable_decimal,
                CA_PROP_CANBERRA_XDG_THEME_NAME, sound_theme_name_,
                CA_PROP_CANBERRA_VOLUME, volume_db_decimal,
                NULL);
        if (err != 0)
                warn("setting libcanberra properties: %s", ca_strerror(err));

        ca_props_changed_ = false;
}

/* Set KEY to VALUE in PROPLIST if VALUE is not null. Exit with an error
   if the value cannot be set. */
static void
xsetprop(ca_proplist *proplist, char const *key, char const *value)
{
        if (!value)
                return;
        int err = ca_proplist_sets(proplist, key, value);
        if (err != 0)
                die("setting proplist entry %s: %s\n", key,
                    ca_strerror(err));
}

/* Play the bell sound. */
static void
ring_bell(void)
{
        if (ca_props_changed_) {
                set_canberra_properties();
        }

        if (!bell_proplist_) {
                int err = ca_proplist_create(&bell_proplist_);
                if (err != 0)
                        die("creating proplist: %s", ca_strerror(err));
                xsetprop(bell_proplist_,
                         CA_PROP_EVENT_ID, event_id_);
                xsetprop(bell_proplist_,
                         CA_PROP_MEDIA_FILENAME, event_file_);
                xsetprop(bell_proplist_,
                         CA_PROP_EVENT_DESCRIPTION, event_desc_);
                xsetprop(bell_proplist_,
                         CA_PROP_CANBERRA_CACHE_CONTROL, cache_control_);
        }

        /* Bell! */
        // TODO: cancellation?
        ca_context_play_full(ca_, 0, bell_proplist_, NULL, NULL);
}


/// --- X connection and XKB handling --------------------------------

/* Connect to the X server and pick out our screen and root window. */
static void
init_xcb(void)
{
        xc_ = xcb_connect(NULL, &screen_);
        int err = xcb_connection_has_error(xc_);
        if (err != 0)
                die("opening X connection: xcb error %d", err);

        xcb_screen_iterator_t iter =
                xcb_setup_roots_iterator(xcb_get_setup(xc_));
        if (iter.rem < (screen_ + 1))
                die("examining X screens: not enough screens (%d/%d)?",
                    screen_, iter.rem);
        for (int i = screen_; i > 0; i--) {
                xcb_screen_next(&iter);
        }
        if (!iter.data)
                die("examining X screens: no such screen %d?", screen_);
        root_window_ = iter.data->root;
}

/* Establish awareness of the XKB extension on our X connection and
   select for bell events. */
static void
init_xkb(void)
{
        xcb_query_extension_reply_t const *const query =
                xcb_get_extension_data(xc_, &xcb_xkb_id);
        if (!query->present)
                die("XKB extension not present");
        xkb_event_ = query->first_event;

        xcb_generic_error_t *xerr = NULL;
        xcb_xkb_use_extension_reply_t *const use =
                xcb_xkb_use_extension_reply(
                        xc_, xcb_xkb_use_extension(xc_, 1, 0), &xerr);
        if (!use)
                die_xerr(xerr, "initializing XKB");
        if (!(use->supported && use->serverMajor == 1))
                die("unsupported XKB version: supported=%u version=%u.%u\n",
                    use->supported, use->serverMajor, use->serverMinor);
        debug("XKB server version %u.%u",
              use->serverMajor, use->serverMinor);
        free(use);

        xcb_xkb_select_events(
                /* connection, deviceSpec */
                xc_, XCB_XKB_ID_USE_CORE_KBD,
                /* affectWhich, clear, selectAll */
                XCB_XKB_EVENT_TYPE_BELL_NOTIFY, 0,
                XCB_XKB_EVENT_TYPE_BELL_NOTIFY,
                /* affectMap, map, details */
                0, 0, NULL);
}


/// --- XSETTINGS tracking -------------------------------------------

/* Reset all XSETTINGS-derived settings to their defaults. */
static void
apply_default_xsettings(void)
{
        free(sound_theme_name_);
        sound_theme_name_ = xstrdup("freedesktop");
        enable_event_sounds_ = true;
        ca_props_changed_ = true;
}

/* Set all atom variables needed for XSETTINGS to their proper atoms. */
static void
intern_xsettings_atoms(void)
{
        char selection_buf[sizeof("_XSETTINGS_S") + 10];
        static char const PROPERTY[] = "_XSETTINGS_SETTINGS";
        static char const MANAGER[] = "MANAGER";
        xcb_intern_atom_cookie_t selection_req, property_req;
        xcb_intern_atom_cookie_t manager_req;

        int const selection_len =
                snprintf(selection_buf, sizeof(selection_buf),
                         "_XSETTINGS_S%d", screen_);
        if (!(0 < selection_len
              && (unsigned)selection_len < sizeof(selection_buf)))
                die("internal error(%d): bad snprintf\n", __LINE__);
        selection_req = xcb_intern_atom(
                xc_, false, selection_len, selection_buf);
        property_req = xcb_intern_atom(
                xc_, false, sizeof(PROPERTY) - 1, PROPERTY);
        manager_req = xcb_intern_atom(
                xc_, false, sizeof(MANAGER) - 1, MANAGER);

        xcb_generic_error_t *xerr = NULL;
        xcb_intern_atom_reply_t *reply;

        reply = xcb_intern_atom_reply(xc_, selection_req, &xerr);
        if (!reply)
                die_xerr(xerr, "getting XSettings atoms");
        xsettings_selection_atom_ = reply->atom;
        free(reply);

        reply = xcb_intern_atom_reply(xc_, property_req, &xerr);
        if (!reply)
                die_xerr(xerr, "getting XSettings atoms");
        xsettings_property_atom_ = reply->atom;
        xsettings_type_atom_ = reply->atom;
        free(reply);

        reply = xcb_intern_atom_reply(xc_, manager_req, &xerr);
        if (!reply)
                die_xerr(xerr, "getting XSettings atoms");
        manager_atom_ = reply->atom;
        free(reply);
}

/* Recognize WINDOW as the window to obtain settings from. This does
   not implicitly reread the settings. */
static void
set_xsettings_window(xcb_window_t window)
{
        xsettings_window_ = window;
        debug("XSettings window is now %u", (unsigned)window);

        if (window != XCB_WINDOW_NONE) {
                xcb_change_window_attributes(
                        xc_, window, XCB_CW_EVENT_MASK,
                        (uint32_t[1]){
                                XCB_EVENT_MASK_PROPERTY_CHANGE
                                | XCB_EVENT_MASK_STRUCTURE_NOTIFY
                        });
        } else {
                xcb_change_window_attributes(
                        xc_, root_window_, XCB_CW_EVENT_MASK,
                        (uint32_t[1]){ XCB_EVENT_MASK_STRUCTURE_NOTIFY });
        }
}

/* Select for the _initial_ events and information necessary to participate
   in the desktop's XSETTINGS. The X server is grabbed for the duration
   unless we are running in debug mode (not good to potentially lock up
   input to the debugger if we take a breakpoint in the middle). */
static void
select_xsettings_events(void)
{
        if (!debug_)
                xcb_grab_server(xc_);

        xcb_generic_error_t *xerr = NULL;
        xcb_get_selection_owner_cookie_t const selection_req =
                xcb_get_selection_owner(xc_, xsettings_selection_atom_);
        xcb_get_selection_owner_reply_t *const selection_reply =
                xcb_get_selection_owner_reply(xc_, selection_req, &xerr);
        if (!selection_reply)
                die_xerr(xerr, "getting XSettings selection owner");
        xcb_window_t const owner = selection_reply->owner;
        free(selection_reply);

        set_xsettings_window(owner);

        if (!debug_)
                xcb_ungrab_server(xc_);
}

/* Extract a 32-bit value starting at PTR. Little-endian if BYTE_ORDER is
   zero, big-endian otherwise. */
static uint32_t
fetch32(unsigned char const *ptr, uint8_t byte_order)
{
        if (byte_order == 0) {
                /* Little-endian */
                return (uint32_t)ptr[0]
                        | ((uint32_t)ptr[1] << 8)
                        | ((uint32_t)ptr[2] << 16)
                        | ((uint32_t)ptr[3] << 24);
        } else {
                /* Big-endian */
                return (uint32_t)ptr[3]
                        | ((uint32_t)ptr[2] << 8)
                        | ((uint32_t)ptr[1] << 16)
                        | ((uint32_t)ptr[0] << 24);
        }
}

/* Extract a 16-bit value starting at PTR. Little-endian if BYTE_ORDER is
   zero, big-endian otherwise. */
static uint16_t
fetch16(unsigned char const *ptr, uint8_t byte_order)
{
        if (byte_order == 0) {
                /* Little-endian */
                return (uint16_t)ptr[0] | ((uint16_t)ptr[1] << 8);
        } else {
                /* Big-endian */
                return (uint16_t)ptr[1] | ((uint16_t)ptr[0] << 8);
        }
}

/* Update on having observed the XSETTINGS setting NAME[..NAME_LEN] taking
   on the integer value VALUE. */
static void
scan_xsettings_int(uint32_t value, char const *name, size_t name_len)
{
        debug("XSetting \"%.*s\" (int)", (int)name_len, name);
        static char const ENABLE_SETTING[] = "Net/EnableEventSounds";
        if (name_len == sizeof(ENABLE_SETTING) - 1
            && memcmp(name, ENABLE_SETTING, name_len) == 0) {
                enable_event_sounds_ = value;
                ca_props_changed_ = true;
        }
}

/* Decode an XSETTINGS string value starting at DATA but not extending
   beyond END, then update on having observed the XSETTINGS setting
   NAME[..NAME_LEN] taking on that value. Return the new data pointer
   after the string representation is consumed, or NULL on error.*/
static unsigned char const *
scan_xsettings_string(unsigned char const *data, unsigned char const *end,
                      char const *name, size_t name_len, uint8_t byte_order)
{
        debug("XSetting \"%.*s\" (str)", (int)name_len, name);
        if (end - data < 4)
                goto too_short;
        unsigned const string_len = fetch32(data, byte_order);
        unsigned const padded_len = (string_len + 3) & ~(unsigned)3;
        data += 4;

        if (string_len >= ((unsigned)1 << 20)) {
                warn("decoding XSettings: string too long\n");
                return NULL;
        }
        if (end - data < padded_len)
                goto too_short;
        char const *const string = (char const *)data;
        data += padded_len;

        /* Actually process the setting. */
        static char const SOUND_THEME_SETTING[] = "Net/SoundThemeName";
        if (name_len == sizeof(SOUND_THEME_SETTING) - 1
            && memcmp(name, SOUND_THEME_SETTING, name_len) == 0) {
                free(sound_theme_name_);
                sound_theme_name_ = xmalloc(string_len + 1);
                memcpy(sound_theme_name_, string, string_len);
                sound_theme_name_[string_len] = '\0';
                debug("set sound theme to %s", sound_theme_name_);
                ca_props_changed_ = true;
        }

        return data;

too_short:
        warn("decoding XSettings: blob ended unexpectedly\n");
        return NULL;
}

/* Reconfigure based on the LEN-byte XSETTINGS blob starting at DATA. This
   updates settings incrementally, with any unaffected settings left at
   their previous values. */
static void
scan_xsettings_blob(unsigned char const *data, size_t len)
{
        unsigned char const *const end = data + len;

        if (end - data < 12)
                goto too_short;
        uint8_t const byte_order = data[0];
        uint32_t count = fetch32(&data[8], byte_order);
        data += 12;

        while (count-- > 0) {
                if (end - data < 4)
                        goto too_short;
                uint8_t const type = data[0];
                unsigned const name_len = fetch16(&data[2], byte_order);
                unsigned const padded_len = (name_len + 3) & ~(unsigned)3;
                data += 4;

                if (end - data < padded_len + 4)
                        goto too_short;
                char const *const name = (char const *)data;
                data += padded_len + 4;

                switch (type) {
                case 0:         /* Integer */
                        if (end - data < 4)
                                goto too_short;
                        scan_xsettings_int(
                                fetch32(data, byte_order), name, name_len);
                        data += 4;
                        break;

                case 1:         /* String */
                        if (end - data < 4)
                                goto too_short;
                        data = scan_xsettings_string(
                                data, end, name, name_len, byte_order);
                        if (!data)
                                return;
                        break;

                case 2:         /* Color */
                        if (end - data < 8)
                                goto too_short;
                        debug("XSetting \"%.*s\" (color)",
                              (int)name_len, name);
                        data += 8;
                        break;

                default:
                        warn("decoding XSettings: unknown type %u\n", type);
                        return;
                }
        }

        return;

too_short:
        warn("decoding XSettings: blob ended unexpectedly");
        return;
}

/* Obtain the current XSETTINGS blob from its owner window, then reconfigure
   based on it. Some or all settings may be left at their previous values if
   there is an error obtaining the information. The explicit lack of an
   owner window is not considered an error. */
static void
reread_xsettings(void)
{
        debug("rereading XSettings");
        if (xsettings_window_ == XCB_WINDOW_NONE) {
                /* Note that appendix A of the XSETTINGS 0.5 specification
                   states that if the window drops out of existence,
                   applications should reset to default values for
                   consistency. See the spec for the full rationale. */
                apply_default_xsettings();
                return;
        }

        /* Maximum number of bytes to read. The observed maximum from manual
           testing has been a little under 2 kB, so this is probably enough;
           this process should be lightweight and not take too much of a
           blast radius from overly long settings.

           This should be a multiple of 4, since length in 32-bit words is
           used in the request. */
        static unsigned const BYTES_MAX = 8192;

        xcb_get_property_cookie_t const req =
                xcb_get_property(xc_, false, xsettings_window_,
                                 xsettings_property_atom_,
                                 xsettings_type_atom_,
                                 0, BYTES_MAX / 4);
        xcb_generic_error_t *xerr = NULL;
        xcb_get_property_reply_t *const reply =
                xcb_get_property_reply(xc_, req, &xerr);

        if (!reply) {
                warn_xerr(xerr, "retrieving XSettings property");
                /* This is the only case in which we don't apply the
                   defaults first: a maybe-temporary error retrieving the
                   property may be a bad place to create flickering
                   configuration changes. */
                return;
        }

        int const len = xcb_get_property_value_length(reply);
        if (len < 0) {
                warn("len < 0 while retrieving XSettings property");
                return;
        } else if ((unsigned)len >= BYTES_MAX) {
                warn("XSettings longer than %u bytes might be truncated",
                     BYTES_MAX - 1);
        }

        apply_default_xsettings();
        scan_xsettings_blob(xcb_get_property_value(reply), (size_t)len);
        free(reply);
}

/* Set up our participation in the XSETTINGS system. */
static void
init_xsettings(void)
{
        intern_xsettings_atoms();
        select_xsettings_events();
        apply_default_xsettings();
        reread_xsettings();
}


/// --- Main event loop ----------------------------------------------

/* Process EV, a received XKB event. */
static void
process_xkb_event(xcb_generic_event_t *ev)
{
        // TODO: is there a better way to get the xkbType? This cast is
        // theoretically early.
        xcb_xkb_bell_notify_event_t *const bell =
                (xcb_xkb_bell_notify_event_t *)ev;
        debug("XKB event %u", bell->xkbType);
        if (bell->xkbType != XCB_XKB_BELL_NOTIFY)
                return;

        ring_bell();
}

/* Process EV, a received client message. */
static void
process_client_message(xcb_generic_event_t *ev)
{
        xcb_client_message_event_t *const message =
                (xcb_client_message_event_t *)ev;

        if (message->window == root_window_
            && message->type == manager_atom_
            && message->format == 32
            && message->data.data32[1] == xsettings_selection_atom_) {
                debug("XSettings selection changed ownership");
                set_xsettings_window(message->data.data32[2]);
                reread_xsettings();
        }
}

/* Process EV, a received property change notification. */
static void
process_property_notify(xcb_generic_event_t *ev)
{
        xcb_property_notify_event_t *const prop =
                (xcb_property_notify_event_t *)ev;

        if (prop->window == xsettings_window_
            && prop->atom == xsettings_property_atom_) {
                debug("XSettings property changed");
                reread_xsettings();
        }
}

/* Process EV, a received destroy notification. */
static void
process_destroy_notify(xcb_generic_event_t *ev)
{
        xcb_destroy_notify_event_t *const destroy =
                (xcb_destroy_notify_event_t *)ev;

        if (destroy->window == xsettings_window_) {
                debug("XSettings window destroyed");
                set_xsettings_window(XCB_WINDOW_NONE);
                reread_xsettings();
        }
}

/* Dispatch on incoming events until the cows come home. */
static void
main_loop(void)
{
        xcb_generic_event_t *ev;
        while ((ev = xcb_wait_for_event(xc_))) {
                /* Mask off the artificial-event indicator bit. */
                uint8_t const type = ev->response_type & 0x7f;
                debug("X event %u", type);

                if (type == xkb_event_) {
                        process_xkb_event(ev);
                } else if (type == XCB_CLIENT_MESSAGE) {
                        process_client_message(ev);
                } else if (type == XCB_PROPERTY_NOTIFY) {
                        process_property_notify(ev);
                } else if (type == XCB_DESTROY_NOTIFY) {
                        process_destroy_notify(ev);
                }

                free(ev);
        }
}


/// --- Avoiding accidental simultaneous executions ------------------

/* Whether to use a lock file or not. */
static bool use_lock_file_ = true;
/* Lock file name, generally absolute. Malloc'd. If NULL, automatic. */
static char *lock_file_name_ = NULL;
/* Lock file file descriptor, or -1 if not open. */
static int lock_file_fd_ = -1;

/* Lock the intended lock file if there is one, or do nothing if there
   isn't. Exit if an error occurs during the process.

   This is mainly in case of accidental misconfiguration, where e.g. a user
   shares a home directory between a system that launches this using systemd
   and one that launches it by some other means but forgets to disable the
   second one on the systemd-based system. */
static void
lock_lock_file(void)
{
        if (!use_lock_file_) {
                debug("no lock file");
                return;
        }

        if (!lock_file_name_) {
                char const *const runtime_dir = getenv("XDG_RUNTIME_DIR");
                if (!(runtime_dir && *runtime_dir)) {
                        warn("XDG_RUNTIME_DIR not available;"
                             " no lock file will be used");
                        return;
                }

                int len = asprintf(&lock_file_name_,
                                   "%s/xbell-ringer.lock", runtime_dir);
                if (len < 0)
                        die("asprintf: %s", strerror(errno));
        }

        lock_file_fd_ = open(lock_file_name_, O_RDWR | O_CREAT, 0600);
        if (lock_file_fd_ < 0)
                die("opening lock file %s: %s",
                    lock_file_name_, strerror(errno));

        struct flock lock = {
                .l_type = F_WRLCK,
                .l_whence = SEEK_SET,
                .l_start = 0,
                .l_len = 0
        };

        int err = fcntl(lock_file_fd_, F_SETLK, &lock);
        if (err != 0 && (errno == EACCES || errno == EAGAIN)) {
                err = fcntl(lock_file_fd_, F_GETLK, &lock);
                if (err == 0) {
                        warn("%s already locked by PID %d; exiting",
                             lock_file_name_, lock.l_pid);
                } else {
                        warn("%s already locked; exiting",
                             lock_file_name_);
                }
                exit(EX_TEMPFAIL);
        } else if (err != 0) {
                die("locking lock file: %s", strerror(errno));
        }

        debug("locked %s (fd %d)", lock_file_name_, lock_file_fd_);
        /* Lock successful. */
}


/// --- Main process entrypoint --------------------------------------

/* Values for long options with no short equivalent. */
enum {
        OPTION_DEBUG = 256,
        OPTION_ALLOW_SUPRAUNITY_VOLUME = 257,
        OPTION_LOCK_FILE = 258,
        OPTION_NO_LOCK_FILE = 259,
};

/* Table of long options. */
static struct option const OPTIONS[] = {
        /* name, has_arg, flag, val */
        { "cache-control", 1, NULL, 'c' },
        { "description",   1, NULL, 'd' },
        { "file",          1, NULL, 'f' },
        { "id",            1, NULL, 'i' },
        { "help",          0, NULL, 'h' },
        { "version",       0, NULL, 'v' },
        { "volume",        1, NULL, 'V' },
        { "debug",         0, NULL, OPTION_DEBUG },
        { "allow-supraunity-decibel-volume",
          0, NULL, OPTION_ALLOW_SUPRAUNITY_VOLUME },
        { "lock-file",
          1, NULL, OPTION_LOCK_FILE },
        { "no-lock-file",
          0, NULL, OPTION_NO_LOCK_FILE },
        { NULL,            0, NULL, 0   }
};

/* Verbatim usage string to print. */
static char const USAGE[] =
        "Usage: xbell-ringer [OPTION]...\n"
        "Options:\n"
        "  -c, --cache-control=HOW   Cache played sounds\n"
        "                            (permanent, volatile, never)\n"
        "  -d, --description=DESC    Event description\n"
        "  -f, --file=FILE           Sound file to play\n"
        "  -i, --id=EVENT            Sound event to play\n"
        "  -V, --volume=DBFS         Volume in (negative) decibels\n"
        "  --lock-file=FILE          Use an alternate lock file to guard\n"
        "                            against simultaneous executions\n"
        "  --no-lock-file            Do not use a lock file\n"
        "Help options:\n"
        "  -h, --help                Show this help message\n"
        "  -v, --version             Show the program version\n"
        "Debugging options:\n"
        "  --debug                   Print extra information and adjust\n"
        "                            behavior slightly (see manual page)\n";

/* License trailer to print. */
static char const LICENSE[] =
        "This is free software; you may change and redistribute it under\n"
        "the terms of the GNU GPLv3 or later. There is NO WARRANTY, to\n"
        "the extent permitted by law. See the license file included in\n"
        "this distribution or <https://gnu.org/licenses/gpl.html>.\n";

/* From generated file xbell-ringer-version.c */
extern char const *xbell_ringer_version;
extern char const *xbell_ringer_copyright;

/* Set volume_db_ based on ARG. This only does type checking and not any
   kind of value checking. */
static void
set_volume(char const *arg)
{
        char *end;
        double db = strtod(arg, &end);
        if (*end != '\0') {
                fputs("argument to --volume must be a decimal number\n",
                      stderr);
                fputs(USAGE, stderr);
                exit(EX_USAGE);
        }

        volume_db_ = db;
}

/* Ensure volume_db_ looks reasonable. If ALLOW_SUPRAUNITY_VOLUME is false,
   clamp it to full scale. */
static void
check_volume(bool allow_supraunity_volume)
{
        if (!allow_supraunity_volume && volume_db_ > 0.0) {
                fputs("warning: positive decibels is requesting"
                      " amplification beyond full range!\n", stderr);
                fputs("warning: lowering to +0.0 dB (100%) volume; use"
                      " --allow-supraunity-decibel-volume to override\n",
                      stderr);
                volume_db_ = 0.0;
        }
}

/* Process all our command-line options from ARGV[..ARGC]. */
static void
process_options(int argc, char **argv)
{
        bool allow_supraunity_volume = false;

        int option;
        while ((option = getopt_long(argc, argv, "+c:d:f:i:hvV:",
                                     OPTIONS, NULL)) != -1) {
                switch (option) {
                case '?':
                        fputs(USAGE, stderr);
                        exit(EX_USAGE);
                case 'h':
                        fputs(USAGE, stdout);
                        exit(0);
                case 'v':
                        printf("xbell-ringer %s\n%s\n",
                               xbell_ringer_version,
                               xbell_ringer_copyright);
                        fputs(LICENSE, stdout);
                        exit(0);

                case 'c':
                        cache_control_ = optarg;
                        break;
                case 'd':
                        event_desc_ = optarg;
                        break;
                case 'f':
                        event_file_ = optarg;
                        break;
                case 'i':
                        event_id_ = optarg;
                        break;
                case 'V':
                        set_volume(optarg);
                        break;
                case OPTION_ALLOW_SUPRAUNITY_VOLUME:
                        allow_supraunity_volume = true;
                        break;

                case OPTION_LOCK_FILE:
                        lock_file_name_ = xstrdup(optarg);
                        break;
                case OPTION_NO_LOCK_FILE:
                        use_lock_file_ = false;
                        break;

                case OPTION_DEBUG:
                        debug_ = true;
                        break;

                default:
                        fprintf(stderr, "strange option %d?\n", option);
                        fputs(USAGE, stderr);
                        exit(EX_USAGE);
                }
        }

        check_volume(allow_supraunity_volume);

        if (!event_file_ && !event_id_) {
                event_id_ = "bell";
        } else if (event_file_ && event_id_) {
                event_file_ = NULL;
                fputs("warning: --id overrides --file\n", stderr);
        }
        debug("id=%s, file=%s", event_id_? event_id_ : "(null)",
              event_file_? event_file_ : "(null)");

        if (lock_file_name_ && !use_lock_file_) {
                warn("--no-lock-file overrides --lock-file");
        }
}

/* Main process entrypoint. */
int
main(int argc, char *argv[])
{
        process_options(argc, argv);
        lock_lock_file();
        init_xcb();
        init_xkb();
        init_xsettings();
        set_canberra_properties();
#if WITH_SYSTEMD
        sd_notify(1, "READY=1");
#endif
        main_loop();
        return 0;
}


/// --- Trailer ------------------------------------------------------

// Local variables:
// mode: c
// c-file-style: "linux"
// fill-column: 76
// End:
